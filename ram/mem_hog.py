"""
This script will simply

- Allocate a big array to consume RAM
- Print the RAM usage before and after the allocation

This allows us to have a simple test on the CICD server's RAM
"""
import argparse
import numpy as np
import os
import time

parser = argparse.ArgumentParser()
parser.add_argument(
    "--mem",
    help="(# GiB) you want to hog",
    #nargs="?",
    #const=1.0,
    type=float,
    default=1.0,
)
parser.add_argument(
    "--sustain",
    help="(# seconds) to sustain the program",
    #nargs="?",
    #const=10,
    type=int,
    default=10,
)
args = parser.parse_args()

n_GiB = args.mem
n_bytes = int(n_GiB * (2**10)**3)
shape = (n_bytes // 8,)

os.system("echo '(Before)'; free -h")
A = np.zeros(shape, dtype=np.float64)
# Assigning a variable to np.zeros() does not really put the array onto RAM.
# One needs to further perturbe the entries a bit, e.g. as follows
A += 0

# ===================
#  Alternative
# ===================
# Another simple alternative is to assign a random array, e.g.
# (Note that np.random.normal()'s dtype is default to np.float64)
#A = np.random.normal(size=shape)


n_sec = args.sustain
for i in range(n_sec):
    print(f"This program will terminate in {n_sec-i} sec.\r")
    time.sleep(1)
    if i == 1:
        os.system("echo '(Midway)'; free -h")

del A
os.system("echo '(After)'; free -h")
